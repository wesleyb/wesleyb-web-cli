# Wes CLI - DSL for Websites by Wesley B

This is a requried development dependency for websites by [Wesley B](http://wesleyb.me) that provides a consistent API to build them.

### Install

You can start using Wes CLI in two steps:

1. [`Install`](https://docs.npmjs.com/cli/install), [link](https://docs.npmjs.com/cli/link), or [alias](https://arunmichaeldsouza.com/blog/aliasing-module-paths-in-node-js) the dependency. <details>Examples:</details>
    <summary>
    - Install
        ```
        npm install --save git+ssh://git@bitbucket.org/{user}/{repository}.git
        ```
    - Link
        ```
        cd DIR_WITH_PROJECT_THAT_NEEDS_CLI/wesleyb-web-WEBSITE_NAME
        npm link DIR_WITH_CLI_REPO_CLONE/wesleyb-web-cli
        ```
    </summary>
2. Require the dependency. <details>Examples:</details>
    <summary>
    - In the command line, run `"require('wes-cli');" >> ./index.js`.
    - Create a file `index.js` with the contents `require('wes-cli');`.
    </summary>
3. Use the dependency. <details>Examples:</details>
    <summary>
    - In the command line, run `npx wes-cli OPTIONS_HERE`.
    - Create an NPM script that equals `npm run wes-cli OPTIONS_HERE`.
    </summary>

> This project is __not__ a public NPM package, so you can __not__ install it via `npm -i wes-cli`.

This will allow using `wes-cli` in the command line from within your project.

## Usage

## Command Line

If you'd like to use `wes-cli` on the command line, install it globally with the `-g` flag. The following code can be run either a _Unix_ or _Windows_ command line:

```bash
$ wes-cli --build                                       # Build website (default location)
$ wes-cli --docs --output-dir ./dist/about/docs         # Generate docs to output directory
$ wes-cli --serve --docs --output-dir ./dist/about/docs # Generate docs to output directory and serve it
```

These commands may come in the future:

```bash
$ wes-cli --serve --docs --output-dir ./dist/about/docs # Generate docs to output directory and serve them as a website
```

## Directory Structure

These directories are manually maintained.

    ./
       |_ public              // user-facing files
       |_ scripts             // JavaScript and configuration
       |_ styles              // stylesheets and configuration

## Requirements

- [Node][nodejs] 10.X+ _(do **not** exceed LTS)_
    - [NPM](https://docs.npmjs.com/getting-started/installing-node#updating-npm) 6.X+ _(or latest available for given Node version)_

## Quick Start

Build optimized user-facing static content using the command most appropriate for your host:

- Node: `npm install --unsafe-perm && npm run start`¹

**The result is static user-facing content at `./dist`.**

> ¹ The `--unsafe-perm` flag is only necessary for running the command as root/sudo.

## Development

### Notice

To be effectual, the following commands:

- **must** be run from this (`./`) directory
- **must** have been preceeded by the command `npm install`¹ _at least once_

> ¹ If `npm install` produces an error that mentions `cannot run in wd`, you are likely trying to run the command as root/sudo. If so, add the flag `--unsafe-perm`.

### Commands

#### Start via `npm start`

Run a local instance of the website.

#### Build via `npm run build`

Build static content and put it in `./dist`.

#### Install Dependencies via `npm install`

Install dependencies for all projects and sub-projects.

> If adding a new dependency, then use the flag `--save-dev` (compile time dependency) **or** `--save` (runtime dependency).

## Footnotes

1. For Windows, ensure that `node.exe` is on the `PATH` system variable.
2. Do **not** install dependencies globally.



[nodejs]: https://nodejs.org/ "Node.js"

[eslint]: http://eslint.org/ "ESLint"
[csslint]: https://www.google.com/search?q=css+linter "some CSS linter"

[yi3-style-guides]: http://welseyb.io/developer/style-guides "Yi3: Style Guides"

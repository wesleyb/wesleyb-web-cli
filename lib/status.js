/**
 * Status notification/logging
 * @module status
 */
'use strict';

const ora = require('ora');
const chalk = require('chalk');

/**
 * Simple wrapper for static use of `ora` spinner
 */
class Status {
  // RFE: Support class fields (i.e. `#spinner`), without babel (if possible)
  // FAQ: Node -v 12.x supports class fields, but this script fails in Node land
  constructor() {
    /**
     * The spinner object provided by library
     * @type {String}
     */
    this._spinner;

    /**
     * Text prepended before starting spinner
     * @constant {String}
     */
    this._spinnerPrefix = chalk.bold.magenta('>') + ' ';
  }

  /**
   * Before each method:
   * - Create internal spinner
   * @private
   */
  _beforeEach() {
    this._spinner = this._spinner || ora().start();
  }

  /** Mark start of a process */
  start( text ) {
    this._beforeEach();
    this._spinner.start( this._spinnerPrefix + text ).stopAndPersist();
  }
  /** Mark success of process */
  done( text ) {
    this._beforeEach();
    this._spinner.succeed( text );
  }
  /** Warn about step in process */
  warn( text ) {
    this._beforeEach();
    this._spinner.warn( text );
  }
  /** Warn about step in process */
  info( text ) {
    this._beforeEach();
    this._spinner.info( text );
  }
  /** Mark failure of a process */
  fail( text ) {
    this._beforeEach();
    this._spinner.fail( text );
  }
  /** Mark end of a process */
  end( text ) {
    this._beforeEach();

    if ( options.text ) this._spinner.stopAndPersist({ text: text });
    else this._spinner.stop();
  }
};

module.exports = new Status();
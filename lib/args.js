/**
 * Argument parser
 * @module args
 */
'use strict';

const chalk = require('chalk');
const yargs = require('yargs');

/**
 * CLI logo (ASCII art)
 */
const logo = `
┌────┐
│ ꓭ↗︎ │
└────┘
`

// FAQ: Short name so that logo ASCII art code lines up matching final string
/**
 * CLI version
 */
const vs = require('../package.json').version;

/**
 * CLI version inside CLI logo
 */
const versionLogo = chalk.bold.magenta(`
┌───────────┐
│ ꓭ↗︎ v${vs} │
└───────────┘
`);

module.exports = yargs
  .usage(
    `${chalk.bold.magenta(logo)}
Usage:
$0 [OPTIONS]`
  )
  .group(
    ['b', 'd', 't'],
    'Options (Basic) (only one is allowed):'
  )
  .option('b', {
    alias: 'build',
    desc: 'Build the website',
    type: 'boolean',
    conflicts: ['docs', 'test']
  })
  .option('d', {
    alias: 'docs',
    desc: 'Generate documentation website ',
    type: 'boolean',
    conflicts: ['build', 'test']
  })
  .option('t', {
    alias: 'tests',
    desc: 'Generate test results',
    type: 'boolean',
    conflicts: ['build', 'docs']
  })
  .group(
    ['o', 's', 'verbose', 'debug'],
    'Options (Advanced):'
  )
  .option('o', {
    alias: 'output',
    desc: 'Output directory for an artifact or the entire project',
    type: 'string'
  })
  .option('s', {
    alias: 'serve',
    desc: 'Start webserver or serve artifact via ad hoc webserver',
    type: 'boolean',
  })
  .option('verbose', {
    desc: 'Be verbose',
    type: 'boolean'
  })
  .option('debug', {
    desc: 'Log debug info for this script',
    type: 'boolean'
  })
  .group(
    ['h', 'version'],
    'Options:'
  )
  .version( versionLogo )
  .help()
  .alias('h', 'help')
  .example('$0 --build', 'Build website (default location)')
  .example('$0 --docs', 'Generate docs (default location)')
  .example('$0 --docs --serve', 'Generate docs and serve via ad hoc web service')
  .example('$0 --docs --output-dir ./docs', 'Generate docs to output directory')
  .example('$0 --d --s --output-dir ./docs', 'Generate docs to output directory and serve directory via ad hoc web service')
  .example('$0 --serve', 'Start local web service')
  .epilog(
    "The web project is expected to have the NPM scripts `build`, `docs`, `test`, and `serve`. If any script is missing, and it's respective argument is passed, then an error will occur."
  ).argv;
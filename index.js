/**
 * Main process
 * @module main
 */
'use strict';

const path = require('path');

const chalk = require('chalk');
const shell = require('shelljs-exec-proxy');

const argv = require('./lib/args');
const status = require('./lib/status');

/**
 * Information about the artifact of the build command
 * @typedef {Object} ArtifactLocation
 * @property {String} [path="./"] - The user-provided path to which artifact should exist
 * @property {String} origin - The original location of the artifact
 * @property {String} dest - The final destination of the artifact
 */
const artifact = {
  // BB2: Serve and Output Origin Path Bug
  path: `./`
};

/**
 * Whether user passed a build option
 * @type {Boolean}
 */
let hasBuildOption = false;

{
  configure();
  handleUserError();

  try {
    if ( argv.build ) build();
    if ( argv.test ) test();
    if ( argv.docs ) docs();

    if ( argv.output ) output();
    if ( argv.serve ) serve();
  } catch ( err ) {
    handleError( err );

    process.exit( 1 );
  }
}

/** Configure global settings for this script */
function configure() {
  // RFE: Allow value `false` and user `ctrl + C` (cancel) `--serve` sans error
  // FAQ: This causes `ctrl + C` (cancel) `--serve` to exit with error code
  // SEE: https://github.com/shelljs/shelljs/issues/735 (related, but not exact)
  // shell.config.fatal = true;

  if ( argv.debug ) {
    status.info('Arguments available:');
    console.info( argv );
  }
  if ( argv.verbose ) {
    shell.config.verbose = true;
  }
}

/**
 * Handle errors for user-provided CLI arguments
 */
function handleUserError () {
  const buildOpts = ['build', 'docs', 'test'];
  const buildOptsCount = buildOpts.filter( opt => argv.hasOwnProperty( opt )).length;

  if ( buildOptsCount > 1 ) {
    handleError(
      'Input Error: Cannot use --docs, --build, nor --test together'
    );
  }

  hasBuildOption = ( buildOptsCount === 1 );
}

/**
 * Build the website
 */
function build() {
  // RFE: Make this directory the standard
  // artifact.path = './dist/public';
  artifact.path = './dist';
  runScript('build');
}

/**
 * Run the test cases
 */
function test() {
  artifact.path = './dist/test';
  runScript('test');
}

/**
 * Generate the documentation
 */
function docs() {
  artifact.path = './dist/docs';
  runScript('docs');
}

/**
 * Serve an artifact or start project web service
 */
function serve() {
  runScript('serve');
}

/**
 * Copy an artifact or project to user-defined destination
 */
function output() {
  artifact.origin = path.join( process.env.PWD, artifact.path );
  artifact.dest = path.join( process.env.PWD, argv.output );

  shell.rm('-rf', artifact.dest );
  shell.cp('-r', artifact.origin, artifact.dest );
}

/**
 * Run an NPM script
 * @param {String} commandName - The name of the NPM script to run
 */
function runScript( commandName ) {
  status.start(`Start ${commandName}`);

  // RFE: This should not output when user `ctrl + C` (cancels) script
  executeCommand('npm', ['run', commandName, '--colors']);

  // RFE: This should not output when user `ctrl + C` (cancels) script
  status.done(`Finish ${commandName}`);
}

/**
 * Execute a command on the command line (via a proxy)
 *
 * _This function is secured against [command injection](https://github.com/shelljs/shelljs/wiki/Security-guidelines#exec-command-injection) by [`shell-exec-proxy`](https://github.com/nfischer/shelljs-exec-proxy#security-improvements)._
 * @param {String} command - The command to execute
 * @param {Array} args - The arguments to pass
 */
function executeCommand( command, args ) {
  shell[ command ]( ...args );
}

/**
 * Output an error
 * @param {Error} err
 */
function handleError( err ) {
  // ???: This seems to always print at the end. Why?
  status.fail('Error has occurred, please review output');

  // Seperate error from logging output
  // RFE: Test and understand this code before blindly implementing;
  //      I think it halts output, so the followign errors are distinct from it
  // if ( argv.verbose ) {
  //   console.error();
  // }

  if (typeof err === 'string') {
    console.error( chalk.red( err ));
  } else {
    console.error( err );
  }

  process.exit( 1 );
}